package com.example.test.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Post(
    @Json(name="poster_full")
    val posterFull: Branch,
    val text: String? = null,
    val images: Array<Image>
)

@JsonClass(generateAdapter = true)
data class Image(
    val height: Int,
    val width: Int,
    val image: String
)
