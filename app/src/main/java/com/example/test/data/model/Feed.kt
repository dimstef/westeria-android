package com.example.test.data.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PostResults(
    val next: String? = null,
    val prev: String? = null,
    val results: Array<Post>
)