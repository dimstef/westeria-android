package com.example.test.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Branch(
    val uri: String = "",
    val name: String = "",
    @Json(name="branch_image")
    val branchImage: String = "",
    val children_count: Int,
    val last_day_spread_count: Int? = null,
    val children: Array<String>
)