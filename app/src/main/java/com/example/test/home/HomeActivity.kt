package com.example.test.home

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.test.R
import com.example.test.data.model.Post
import com.example.test.data.model.PostResults
import com.squareup.moshi.Moshi
import kotlinx.android.synthetic.main.activity_home.*
import okhttp3.*
import java.io.IOException

class HomeActivity : AppCompatActivity() {

    private var hasMore: Boolean = true
    private var loading: Boolean = true
    private var next: String? = null
    private lateinit var scrollListener: RecyclerView.OnScrollListener
    private var posts = ArrayList<Post>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fetchData()
        setContentView(R.layout.activity_home)

        recyclerView_main.layoutManager = LinearLayoutManager(this)

        val recyclerViewTouchListener = RecyclerViewTouchListener(this)
        recyclerView_main.addOnItemTouchListener(recyclerViewTouchListener)
        recyclerView_main.adapter = HomeAdapter(posts)

        setRecyclerViewScrollListener()
    }

    private fun setRecyclerViewScrollListener() {
        scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                val lastVisibleItemPosition = (recyclerView_main.layoutManager as LinearLayoutManager)
                    .findLastVisibleItemPosition()
                super.onScrollStateChanged(recyclerView, newState)
                val totalItemCount = recyclerView?.layoutManager?.itemCount
                Log.d("MyTAG", "Load new list $lastVisibleItemPosition $totalItemCount")
                if (totalItemCount == lastVisibleItemPosition + 1 && !loading) {
                    Log.d("MyTAG", "Load new list")
                    fetchData()
                    //recyclerView_main.removeOnScrollListener(scrollListener)
                }
            }
        }

        recyclerView_main.addOnScrollListener(scrollListener)
    }

    fun fetchData(){
        loading = true
        val url = next?:"https://subranch.com/api/posts/all/"
        val request = Request.Builder().url(url).build()

        val client = OkHttpClient()
        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call, response: Response) {
                var body = response.body?.string()
                var moshi = Moshi.Builder().build()
                var jsonAdapter = moshi.adapter<PostResults>(PostResults::class.java)
                val results = jsonAdapter.fromJson(body)
                val prevSize = posts.size
                posts.addAll(results?.results!!)
                next = results?.next!!
                loading = false

                runOnUiThread{
                    recyclerView_main.adapter?.notifyItemRangeInserted(posts.size,prevSize + 1)
                }
            }
            override fun onFailure(call: Call, e: IOException) {
                println("failed to request")
            }
        })
    }
}

