package com.example.test.home

import android.content.Context
import android.view.*
import androidx.core.view.GestureDetectorCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.test.R
import com.example.test.data.model.Post
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.content_post_row.view.*
import kotlin.math.atan2


class HomeAdapter(val feed: ArrayList<Post>): RecyclerView.Adapter<CustomViewHolder>(){
    override fun getItemCount(): Int {
        return feed.size
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.content_post_row,parent,false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.textView_post_text.text = feed[position].text
        holder.view.textView_poster_name.text = feed[position].posterFull.name
        var imageView = holder.view.imageView

        // Set profile image
        val branchProfileImageView = holder.view.imageView_branch_image
        Picasso.get().load(feed[position].posterFull.branchImage).fit()
            .centerCrop()
            .into(branchProfileImageView)

        // Set post images
        if(feed[position].images.isNotEmpty()){
            imageView.layoutParams.height = feed[position].images[0].height
            Picasso.get().load(feed[position].images[0].image).fit()
                .centerCrop()
                .into(imageView)
        }else {
            imageView.setImageDrawable(null)
            Picasso.get().cancelRequest(imageView)
        }
    }
}

class CustomViewHolder(val view: View): RecyclerView.ViewHolder(view)

class RecyclerViewTouchListener(private val context: Context): RecyclerView.OnItemTouchListener{
    private var originalX = 0.0F
    private var angle = 1000.0F
    private val gestureDetector: GestureDetectorCompat
    init {
        gestureDetector = GestureDetectorCompat(context, object : GestureDetector.SimpleOnGestureListener() {
            override fun onScroll(
                e1: MotionEvent,
                e2: MotionEvent,
                distanceX: Float,
                distanceY: Float
            ): Boolean {
                angle = Math.toDegrees(
                    atan2(
                        e1?.y - e2?.y,
                        e2?.x - e1?.x
                    ).toDouble()
                ).toFloat()
                return super.onScroll(e1, e2, distanceX, distanceY)
            }
        })
    }


    override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
        val child = rv.findChildViewUnder(e.x, e.y)
        rv.requestDisallowInterceptTouchEvent(true)

        when (e.action) {
            MotionEvent.ACTION_UP->{
                child?.animate()?.x(0.0F)
                    ?.setDuration(500)
                    ?.start()
            }
            MotionEvent.ACTION_MOVE ->{
                child?.animate()?.x(e.x - originalX)
                    ?.setDuration(0)
                    ?.start()
            }
        }
    }

    override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
        gestureDetector.onTouchEvent(e)

        when (e.action) {
            MotionEvent.ACTION_DOWN->{
                angle = 1000.0F
                originalX = e.x
                return false
            }
            MotionEvent.ACTION_UP->{
                angle = 1000.0F
                return false
            }
            MotionEvent.ACTION_CANCEL->{
                angle = 1000.0F
                return false
            }
            MotionEvent.ACTION_MOVE ->{

                if(angle > -45 && angle <= 45 ||
                    angle >= 135 && angle < 180 || angle < -135 && angle > -180 && angle!=1000.0F) {
                    return true
                }
            }
        }
        return false
    }

    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {

    }
}

class CustomLayoutManager(context: Context?) :
    LinearLayoutManager(context) {
    private var isScrollEnabled = true
    fun setScrollEnabled(flag: Boolean) {
        isScrollEnabled = flag
    }

    override fun canScrollVertically(): Boolean { //Similarly you can customize "canScrollHorizontally()" for managing horizontal scroll
        return isScrollEnabled && super.canScrollVertically()
    }
}